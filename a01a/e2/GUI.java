package a01a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
    
    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    private final Map<JButton, Pair<Integer, Integer>> buttons = new HashMap<>();
    private final Logic logics;
    private static final int SIZE = 5;

    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300,300);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        logics=new LogicImpl(SIZE);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            if(logics.hit(buttons.get(bt).getX(), buttons.get(bt).getY()))
            {
                for (JButton b : buttons.keySet()) {
                    if(b.getText()=="K")
                    {
                        b.setText(" ");
                        break;
                    }
                }
                bt.setText("K");
                if(logics.isOver())
                {
                    System.exit(1);
                }
            }
            
        };
        
        for (int i=0; i<SIZE; i++){
            for(int j=0;j<SIZE;j++) {
                final JButton jb = new JButton(setText(i,j));
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<Integer, Integer>(i, j));
                panel.add(jb);
            }
        }
        
        
        
        
        
        this.setVisible(true);
      }

    private String setText(int i, int j) {
        return (i == logics.getKX() && j == logics.getKY() ? "K"
                : (i == logics.getPedX() && j == logics.getPedY() ? "*" : " "));
    }
    
}
