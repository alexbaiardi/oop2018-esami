package a01a.e2;

public interface Logic {
    
    int getKX();
    int getKY();
    int getPedX();
    int getPedY();
    
    boolean hit(int x,int y);
    boolean isOver();  

}
