package a01a.e2;

import java.util.Random;

public class LogicImpl implements Logic {

    private Pair<Integer,Integer> k;
    private final Pair<Integer,Integer> ped;
    
    public LogicImpl(int size) {
        Random r=new Random();
        this.k = new Pair<>(r.nextInt(size), r.nextInt(size));
        this.ped = new Pair<>(r.nextInt(size), r.nextInt(size));
    }

    @Override
    public int getKX() {
        return k.getX();
    }

    @Override
    public int getKY() {
        return k.getY();
    }

    @Override
    public int getPedX() {
        return ped.getX();
    }

    @Override
    public int getPedY() {
        return ped.getY();
    }

    @Override
    public boolean hit(int x, int y) {
        if ((Math.abs(k.getX() - x) == 2 && Math.abs(k.getY() - y) == 1)
                || (Math.abs(k.getX() - x) == 1 && Math.abs(k.getY() - y) == 2)) {
            k= new Pair<>(x,y);
            return true;
        }
        return false;
    }

    @Override
    public boolean isOver() {
        return k.equals(ped);
    }

}
