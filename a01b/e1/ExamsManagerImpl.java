package a01b.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collector;

import com.sun.net.httpserver.Filter;

public class ExamsManagerImpl implements ExamsManager {

    Map<String, Set<Pair<String, ExamResult>>> courseResult= new HashMap<>();
    
    @Override
    public void createNewCall(String call) {
        if(courseResult.containsKey(call)) {
            throw new IllegalArgumentException();
        }
        courseResult.put(call,new HashSet<>());
    }

    @Override
    public void addStudentResult(String call, String student, ExamResult result) {
        courseResult.get(call).add(new Pair<String, ExamResult>(student, result));
    }

    @Override
    public Set<String> getAllStudentsFromCall(String call) {
        Set<String> res=new HashSet<String>();
        courseResult.get(call).stream().forEach(p->res.add(p.getFst()));
        return res;
    }

    @Override
    public Map<String, Integer> getEvaluationsMapFromCall(String call) {
        Map<String,Integer> res= new HashMap<String, Integer>();
        courseResult.get(call).stream().filter(p->p.getSnd().getEvaluation().isPresent()).forEach(pa->res.put(pa.getFst(),pa.getSnd().getEvaluation().get()));
        return res;
    }

    @Override
    public Map<String, String> getResultsMapFromStudent(String student) {
        Map<String,String> res= new HashMap<String, String>();
        //courseResult.entrySet().stream().flatMap(e->new Pair(e.getKey(),e.getValue().stream().filter(e->e.getFst().equals(student)).map(p->p.getSnd().toString()).findFirst().get()));
        return res;
    }

    @Override
    public Optional<Integer> getBestResultFromStudent(String student) {
        return courseResult.values().stream().flatMap(Set::stream).filter(p->p.getFst().equals(student)).filter(p->p.getSnd().getEvaluation().isPresent()).map(p->p.getSnd().getEvaluation().get()).max((x,y)->x-y);
    }

}
