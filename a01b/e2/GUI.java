package a01b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {

private static final int SIZE=5;
private final Map<JButton,Pair<Integer, Integer>> buttons = new HashMap<JButton,Pair<Integer, Integer>>();
private final Logic logics=new LogicImpl();  

    public GUI() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(SIZE*100,SIZE*100);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            if (logics.hit(buttons.get(bt).getX(), buttons.get(bt).getY())) {
                System.exit(0);
            }
            draw();
        };

        for (int i = 0; i < SIZE; i++) {
            for(int j=0;j<SIZE;j++) {
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(i,j));
                panel.add(jb);
            }
        }
        draw();
        this.setVisible(true);
    }
    private void draw() {
        buttons.forEach((k, v) -> {
            k.setEnabled(logics.isEnabled(v.getX(), v.getY()));
            k.setText(logics.hasBishop(v.getX(), v.getY()) ? "B" : " ");
        });
    }
    
}
