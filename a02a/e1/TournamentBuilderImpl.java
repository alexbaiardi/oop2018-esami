package a02a.e1;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import a02a.e1.Tournament.Result;
import a02a.e1.Tournament.Type;

public class TournamentBuilderImpl implements TournamentBuilder {

	private Type type;
	private String name;
	private Map<String, Integer> initialRanking;
	private Map<String, Result> results=new HashMap<>();
	
	@Override
	public TournamentBuilder setType(Type type) { 
		this.type=type;
		return this;
	}

	@Override
	public TournamentBuilder setName(String name) {
		this.name=name;
		return this;
	}

	@Override
	public TournamentBuilder setPriorRanking(Map<String, Integer> ranking) {
		this.initialRanking=ranking;
		return this;
	}

	@Override
	public TournamentBuilder addResult(String player, Result result) {
		checkCondition(this.type!=null);
		if(result==Result.WINNER) {
			checkCondition(!results.containsValue(Result.WINNER));
		}
		checkCondition(results.put(player, result)==null);
		return this;
	}

	@Override
	public Tournament build() {
		checkCondition(name!=null);
		checkCondition(initialRanking!=null);
		if(type==null)
		{
			throw new NullPointerException();
		}
		return new Tournament() {
			
			@Override
			public String winner() {
				return results.entrySet().stream().filter(e->e.getValue().equals(Result.WINNER)).findFirst().get().getKey();
			}
			
			@Override
			public Map<String, Integer> resultingRanking() {
				Map<String, Integer> res=new HashMap<String, Integer>();
				results.entrySet().stream().forEach(e->res.put(e.getKey(),initialRanking.getOrDefault(e.getKey(),0)+calculatePoints(e.getValue())));
				return res;
			}
			
			@Override
			public List<String> rank() {
				return resultingRanking().entrySet()
						.stream()
						.sorted((e1,e2)->e2.getValue()-e1.getValue()).map(e->e.getKey())
						.collect(Collectors.toList());
			}
			
			@Override
			public Map<String, Integer> initialRanking() {
				return initialRanking;
			}
			
			@Override
			public Type getType() {
				return type;
			}
			
			@Override
			public Optional<Result> getResult(String player) {
				return results.entrySet()
						.stream()
						.filter(e->e.getKey().equals(player))
						.map(Entry::getValue)
						.findFirst();
			}
			
			@Override
			public String getName() {
				return name;
			}
		};
	}
	
	private int calculatePoints(Result result) {
		int points=0;
		switch (this.type) {
		case MAJOR:
			points=2500;
			break;
		case ATP1000:
			points=1000;
			break;
		case ATP500:
			points=500;
			break;
		case ATP250:
			points=250;			
		default:
			break;
		}
		switch (result) {
		case WINNER:
			break;
		case FINALIST:
			points*=0.5;
			break;
		case SEMIFINALIST:
			points*=0.2;
			break;
		case QUARTERFINALIST:
			points*=0.1;
		default:
			points=0;
			break;
		}
		
		return points;
	}
	
	private void checkCondition(boolean condition) {
		if(!condition) {		
			throw new IllegalStateException();
		}
	}

}
