package a02b.e1;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RankingImpl implements Ranking {

	
	List<Tournament> tournaments;
	Tournament lastTournament;
	
	public RankingImpl() {
		tournaments=new LinkedList<Tournament>();
		TournamentFactory tf=new TournamentFactoryImpl();
		lastTournament=tf.make(null, 0, 0, null, null);
	}
	
	@Override
	public void loadTournament(Tournament tournament) {
		check(tournaments.size()==0 ||
				this.getCurrentYear()<tournament.getYear() || 
				(this.getCurrentYear()==tournament.getYear()&& 
				getCurrentWeek()<=tournament.getWeek()));
		check(lastTournament.getName()!=tournament.getName());
		tournaments.add(tournament);
		lastTournament=tournament;
	}

	@Override
	public int getCurrentWeek() {
		check(tournaments.size()!=0);
		return lastTournament.getWeek();
	}

	@Override
	public int getCurrentYear() {
		check(tournaments.size()!=0);
		return lastTournament.getYear();
	}

	@Override
	public Integer pointsFromPlayer(String player) {
		return 	lastYearTournaments().map(t->t.getResult(player))
				.filter(p->p.isPresent())
				.map(o->o.get())
				.reduce((x,y)->x+y)
				.orElseGet(()->0);
	}

	@Override
	public List<String> ranking() {
		Set<String> rank= new HashSet<>();
		tournaments.stream().map(t->t.getPlayers()).forEach(e->rank.addAll(e));
		return rank.stream().map(s->new Pair<String, Integer>(s,this.pointsFromPlayer(s))).sorted((p1,p2)->p2.getY()-p1.getY()).map(p->p.getX()).collect(Collectors.toList());
	}

	@Override
	public Map<String, String> winnersFromTournamentInLastYear() {
		Map<String,String> winners=new HashMap<String, String>();
		lastYearTournaments().forEach(t-> winners.put(t.getName(),t.winner()));
		return winners;
	}

	@Override
	public Map<String, Integer> pointsAtEachTournamentFromPlayer(String player) {
		Map<String,Integer> tournamentPlay=new HashMap<>();
		tournaments.stream().filter(t->t.getPlayers().contains(player)).forEach(t->tournamentPlay.put(t.getName(), t.getResult(player).get()));
		return tournamentPlay;
	}

	@Override
	public List<Pair<String, Integer>> pointsAtEachTournamentFromPlayerSorted(String player) {
		return tournaments.stream().filter(t->t.getPlayers().contains(player))
				.sorted((t1,t2)->t1.getYear()*52+t1.getWeek()-t2.getYear()*52+t2.getWeek())
				.map(e->new Pair<>(e.getName(),e.getResult(player).get()))
				.collect(Collectors.toList());
	}
	
	private void check(boolean condition) {
		if(!condition)
		{
			throw new IllegalStateException();
		}		
	}
	
	private Stream<Tournament> lastYearTournaments(){
		return tournaments.stream()
				.dropWhile(t->t.getYear()<lastTournament.getYear()-1||t.getWeek()<lastTournament.getWeek()+1&&t.getYear()==lastTournament.getYear()-1);
	}

}
