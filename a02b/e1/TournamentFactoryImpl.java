package a02b.e1;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class TournamentFactoryImpl implements TournamentFactory {

	@Override
	public Tournament make(String name, int year, int week, Set<String> players, Map<String, Integer> points) {
		return new Tournament() {
			
			private final String tName=name;
			private final int tYear=year;
			private final int tWeek=week;
			private final Set<String> tPlayers=players;
			private final Map<String, Integer> results=points;

			@Override
			public String getName() {
				return tName;
			}

			@Override
			public int getYear() {
				return tYear;
			}

			@Override
			public int getWeek() {
				return tWeek;
			}

			@Override
			public Set<String> getPlayers() {
				return tPlayers;
			}

			@Override
			public Optional<Integer> getResult(String player) {	
				if(results.containsKey(player)) {
					return Optional.of(results.get(player));
				}
				if(tPlayers.contains(player)) {
					return Optional.of(0);
				}	
				return Optional.empty();
			}

			@Override
			public String winner() {
				return results.entrySet().stream().max((e1,e2)->e1.getValue()-e2.getValue()).get().getKey();
			}};
	}

}
