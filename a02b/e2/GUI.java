package a02b.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.swing.*;

public class GUI extends JFrame {
	private static final long serialVersionUID = 1L;
	private static final int SIZE=10;
	private final Map<JButton,Pair<Integer,Integer>> buttons = new HashMap<>();
	private final Map<Pair<Integer,Integer>,JButton> coords = new HashMap<>();
	private final Logic logics=new LogicImpl(SIZE);
	private boolean firstHit=false;
	
    public GUI() {
    	
            this.setSize(SIZE*50,SIZE*50);
            
            JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
            this.getContentPane().add(BorderLayout.CENTER,panel);
            
            ActionListener al = (e)->{
                final JButton bt = (JButton)e.getSource();
                Pair<Integer, Integer> coord=buttons.get(bt);
                if(!firstHit) {
                		logics.hit1(coord.getX(), coord.getY());
                		firstHit=true;
                		bt.setEnabled(false);
                } else {
                	if(logics.hit2(coord.getX(),coord.getY())) {
                		System.exit(0);
                	}
                	draw(logics.rectangleDraw());
                	firstHit=false;
                }
            };
                    
            for (int i=0; i<SIZE; i++){
                for (int j=0; j<SIZE; j++){
                    final JButton jb = new JButton(" ");
                    jb.addActionListener(al);
                    this.buttons.put(jb,new Pair<>(i, j));
                    this.coords.put(new Pair<>(i, j),jb);
                    panel.add(jb);
                }
            }
            this.setVisible(true);
        }

	private void draw(Iterable<Pair<Integer, Integer>> rectangleDraw) {
		for(Pair<Integer, Integer> p : rectangleDraw) {
			coords.get(p).setText("*");
		}
	}
        
}
