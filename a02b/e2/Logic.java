package a02b.e2;

public interface Logic {
	
	void hit1(int x,int y);
	boolean hit2(int x,int y);
	
	Iterable<Pair<Integer, Integer>> rectangleDraw();

}
