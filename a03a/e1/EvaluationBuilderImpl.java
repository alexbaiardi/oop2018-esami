package a03a.e1;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import a03a.e1.Evaluation.Question;
import a03a.e1.Evaluation.Result;

public class EvaluationBuilderImpl implements EvaluationBuilder {

	private Map<Pair<Integer,String>,Map<Question,Result>> evaluetions=new HashMap<>();
	private boolean built=false;
	@Override
	public EvaluationBuilder addEvaluationByMap(String course, int student, Map<Question, Result> results) {
		//check(!built);
		if(results.size()!=3) {
			throw new IllegalArgumentException();
		}
		check(!evaluetions.containsKey(new Pair<>(student,course)));
		this.evaluetions.put(new Pair<>(student, course),results);
		
		return this;
	}

	@Override
	public EvaluationBuilder addEvaluationByResults(String course, int student, Result resOverall, Result resInterest,
			Result resClarity) {
		Map<Question, Result> results=new HashMap<>();
		results.put(Question.OVERALL, resOverall);
		results.put(Question.INTEREST, resInterest);
		results.put(Question.CLARITY, resClarity);
		addEvaluationByMap(course, student, results);
		return this;
	}

	@Override
	public Evaluation build() {
		check(!built);
		built=true;
		return new Evaluation() {
			
			@Override
			public Map<Result, Long> resultsCountForStudent(int student) {
				Map<Result, Long> map=new HashMap<Evaluation.Result, Long>();
				for (Result r: Evaluation.Result.values()) {
					map.put(r, evaluetions.entrySet().stream().filter(e->e.getKey().getX().equals(student))
							.map(e->e.getValue().entrySet().stream().filter(s->s.getValue().equals(r)).count()).reduce((x,y)->x+y).get());
				}
				return map;
			}
			
			@Override
			public Map<Result, Long> resultsCountForCourseAndQuestion(String course, Question questions) {
				Map<Result, Long> map=new HashMap<Evaluation.Result, Long>();
				for (Result r: Evaluation.Result.values()) {
					map.put(r, evaluetions.entrySet().stream()
							.filter(e->e.getKey().getY().equals(course))
							.filter(e->e.getValue().get(questions).equals(r))
							.count()
					);
				}
				return map;
			}
			
			@Override
			public Map<Question, Result> results(String course, int student) {
				if(evaluetions.containsKey(new Pair<>(student,course))){
					return evaluetions.get(new Pair<>(student, course));
				}
				return new HashMap<>();
			}
			
			@Override
			public double coursePositiveResultsRatio(String course, Question question) {
				Map<Result, Long> map=resultsCountForCourseAndQuestion(course, question);
				return (double)(map.get(Result.FULLY_POSITIVE)+map.get(Result.WEAKLY_POSITIVE))/map.entrySet().stream().map(Entry::getValue).reduce((x,y)->x+y).get();
			}
		};
	}
	
	private void check(boolean condition) {
		if(!condition){
			throw new IllegalStateException();
		}		
	}

}
