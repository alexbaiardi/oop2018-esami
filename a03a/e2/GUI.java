package a03a.e2;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;

import javax.swing.*;



public class GUI extends JFrame {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Map<JButton,Pair<Integer,Integer>> buttons=new HashMap<>();
	private final Logic logic=new LogicImpl(SIZE);
	private static final int SIZE=6; 
    
    public GUI() {
    	this.setSize(SIZE*100,SIZE*100);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            logic.hit(buttons.get(bt).getX(), buttons.get(bt).getY());
            if (logic.isOver()){
            	System.exit(0);
            }
            draw();
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                this.buttons.put(jb,new Pair<>(i, j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }
    
    private void draw() {
    	int res;
    	for (JButton bt : buttons.keySet()) {
    		res=logic.getvalue(buttons.get(bt).getX(), buttons.get(bt).getY());
    		if(res!=0) {
    			bt.setText(String.valueOf(res));
    		}
    		if(res==5) {
    			bt.setEnabled(false);
    		}
		}
    }
        
}
