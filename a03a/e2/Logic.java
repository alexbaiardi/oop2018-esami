package a03a.e2;

public interface Logic {
	
	boolean isOver();
	void hit(int x, int y);
	int getvalue(int x, int y);

}
