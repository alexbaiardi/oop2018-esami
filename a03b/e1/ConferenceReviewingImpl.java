package a03b.e1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

public class ConferenceReviewingImpl implements ConferenceReviewing {

    private final Map<Integer,List<Map<Question, Integer>>> rewiews=new HashMap<>();
    
    @Override
    public void loadReview(int article, Map<Question, Integer> scores) {
        rewiews.computeIfAbsent(article,k->new ArrayList<>());
        rewiews.compute(article, (k,v)->{
                                            v.add(scores); 
                                            return v;
                                        });
    }

    @Override
    public void loadReview(int article, int relevance, int significance, int confidence, int fin) {
        Map<Question, Integer> scores= new HashMap<>();
        scores.put(Question.RELEVANCE, relevance);
        scores.put(Question.SIGNIFICANCE, significance);
        scores.put(Question.CONFIDENCE, confidence);
        scores.put(Question.FINAL, fin);
        loadReview(article, scores);
    }

    @Override
    public List<Integer> orderedScores(int article, Question question) {
        return rewiews.entrySet().stream()
                .filter(e->e.getKey().equals(article))
                .findFirst()
                .get()
                .getValue()
                .stream()
                .flatMap(e->e.entrySet().stream().filter(v->v.getKey().equals(question)))
                .mapToInt(e->e.getValue())
                .sorted()
                .boxed()
                .collect(Collectors.toList());
    }

    @Override
    public double averageFinalScore(int article) {
        return rewiews.entrySet().stream()
                .filter(e->e.getKey().equals(article))
                .findFirst()
                .get()
                .getValue()
                .stream()
                .flatMap(e->e.entrySet().stream().filter(v->v.getKey().equals(Question.FINAL)))
                .mapToInt(e->e.getValue())
                .average().getAsDouble();
    }

    @Override
    public Set<Integer> acceptedArticles() {
        return rewiews.entrySet().stream()
                .filter(e->averageFinalScore(e.getKey())>5)
                .filter(e->e.getValue().stream().filter(p->p.get(Question.RELEVANCE)>=8).count()>0)
                .map(Entry::getKey)
                .collect(Collectors.toSet());
    }

    @Override
    public List<Pair<Integer, Double>> sortedAcceptedArticles() {
        List<Pair<Integer, Double>> articles=new ArrayList<>();
        acceptedArticles().stream().forEach(i->articles.add(new Pair<>(i, averageFinalScore(i))));
        articles.sort((p1,p2)->Double.compare(p1.getY(), p2.getY()));
        return articles;
    }

    @Override
    public Map<Integer, Double> averageWeightedFinalScoreMap() {
        return rewiews.entrySet().stream().collect(Collectors.toMap(e->e.getKey(), l->l.getValue().stream().mapToDouble(m->(double)m.get(Question.CONFIDENCE)*m.get(Question.FINAL)/10).average().getAsDouble()));
    }

}
