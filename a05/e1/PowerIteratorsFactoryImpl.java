package a05.e1;

import java.util.List;
import java.util.Optional;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PowerIteratorsFactoryImpl implements PowerIteratorsFactory {

    @Override
    public PowerIterator<Integer> incremental(int start, UnaryOperator<Integer> successive) {
        return new IntStream.iterate.iterate(start,t->successive.apply(t)).iterator();
    }

    @Override
    public <X> PowerIterator<X> fromList(List<X> list) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PowerIterator<Boolean> randomBooleans(int size) {
        // TODO Auto-generated method stub
        return null;
    }

}
