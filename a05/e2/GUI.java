package a05.e2;

import javax.swing.*;
import java.util.*;
import java.util.List;
import java.util.Map.Entry;
import java.awt.*;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    
    private static final long serialVersionUID = -1L;
    private final static int SIZE = 4;
    private final Map<JButton,Pair<Integer, Integer>> buttons = new HashMap<>();
    private final Logic logic;
    
    public GUI() {
        
        logic=new LogicImpl(SIZE);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(100*SIZE, 100*SIZE);
        
        JPanel panel = new JPanel(new GridLayout(SIZE,SIZE));
        this.getContentPane().add(BorderLayout.CENTER,panel);
        
        ActionListener al = (e)->{
            final JButton bt = (JButton)e.getSource();
            Pair<Integer,Integer> coord= buttons.get(bt);
            if(logic.hit(coord.getX(), coord.getY())) {
                bt.setEnabled(false);
                bt.setText(String.valueOf(logic.getValue(coord.getX(), coord.getY())));
                if(logic.isOver()) {
                    System.exit(0);
                }
            }
        };
                
        for (int i=0; i<SIZE; i++){
            for (int j=0; j<SIZE; j++){
                final JButton jb = new JButton(" ");
                jb.addActionListener(al);
                buttons.put(jb,new Pair<>(i, j));
                panel.add(jb);
            }
        }
        this.setVisible(true);
    }
}
