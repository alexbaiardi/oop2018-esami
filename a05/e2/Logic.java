package a05.e2;

public interface Logic {
    
    boolean hit(int x,int y);
    
    boolean isOver();
    
    int getValue(int x,int y);

}
