package a05.e2;

import java.util.LinkedList;

public class LogicImpl implements Logic {

    private final LinkedList<Pair<Integer,Integer>> selected;
    private final int size;
    
    
    public LogicImpl(int size) {
        this.selected = new LinkedList<Pair<Integer,Integer>>();
        this.size = size;
    }

    @Override
    public boolean hit(int x, int y) {
        Pair<Integer,Integer> coord=new Pair<>(x, y);
        if(selected.isEmpty()|| adjacent(coord, selected.getLast())) {
            selected.add(coord);
            return true;
        }
        return false;
    }

    @Override
    public boolean isOver() {
        Pair<Integer,Integer> coord=new Pair<>(selected.getLast().getX(),selected.getLast().getY());
        for(int i=Math.max(0, coord.getX()-1);i<=Math.min(coord.getX()+1,size-1);i++) {
            for(int j=Math.max(0, coord.getY()-1);j<=Math.min(coord.getY()+1,size-1);j++) 
            {
                if(!selected.contains(new Pair<>(i, j))) {
                    return false;
                }
            }
        }
        
        return true;
    }

    @Override
    public int getValue(int x, int y ) {
        return selected.indexOf(new Pair<>(x, y));
    }
    
    private boolean adjacent(Pair<Integer,Integer> p1,Pair<Integer,Integer> p2) {
        if(Math.abs(p1.getX()-p2.getX())<=1&&Math.abs(p1.getY()-p2.getY())<=1) {
            return true;
        }
        return false;
    }

}
