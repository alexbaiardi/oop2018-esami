package a06.e2;

import java.awt.*;
import java.awt.event.ActionListener;
import java.util.stream.*;
import javax.swing.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GUI extends JFrame{

	private final Map<JButton, Integer> buttons;
	private Logic logic;
	
	public GUI(int size){
		
		buttons=new HashMap<>();
		logic=new LogicImpl(size);
		this.setSize(500, 100);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.getContentPane().setLayout(new FlowLayout());
		

		ActionListener ac = e -> {
			final JButton jb = (JButton) e.getSource();
			int index=buttons.get(jb);
			logic.hit(index);
			jb.setEnabled(false);
			jb.setText(String.valueOf(logic.getValue(index)));
		};

		ActionListener ar = e -> {
			logic = new LogicImpl(size);
			initializeButton();
		};

		for (int i = 0; i < size; i++) {
			JButton bt = new JButton();
			bt.addActionListener(ac);
			this.getContentPane().add(bt);
			buttons.put(bt, i);
		}
		initializeButton();
		JButton reset = new JButton("Reset");
		reset.addActionListener(ar);
		this.getContentPane().add(reset);

		this.setVisible(true);

	}

	private void initializeButton() {
		
		for(JButton bt:buttons.keySet()){
			bt.setText(String.valueOf(logic.getValue(buttons.get(bt))));
			bt.setEnabled(true);
		}
		
	}
	
}
